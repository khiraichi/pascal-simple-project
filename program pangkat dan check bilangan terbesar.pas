program pangkat_dan_nilai_tertinggi;
uses crt;
var
    angka_awal:integer;
    angka_pangkat:integer;
    nilai_terbesar:integer;
    function total_pangkat(x:integer):real;
    begin
        total_pangkat:=exp(angka_pangkat*ln(angka_awal));
    end;

begin
    clrscr;
    writeln('Input bilangan pertama : ');readln(angka_awal);
    writeln('Input bilangan kedua : ');readln(angka_pangkat);
    if(angka_pangkat > angka_awal) then
    begin
        nilai_terbesar:=angka_pangkat;
    end
    else if (angka_awal > angka_pangkat) then
    begin
        nilai_terbesar:=angka_awal;
    end
    else 
    begin
        nilai_terbesar:=angka_pangkat;
    end;
    writeln('============================');
    writeln('Nilai ', angka_awal, ' pangkat ', angka_pangkat, ' adalah ', total_pangkat(angka_pangkat):2:0);
    writeln('Nilai terbesar dari dua bilangan tersebut adalah ', nilai_terbesar);
end.