program perkalian_angka_dalam_array;
uses crt;
var
    total_angka:integer;
    array_angka: array[1..4] of integer;
    input_angka:integer;
    i:integer;
    x:integer;
    nilai:integer;
begin
    clrscr;
    i:=0;
    while (i < Length(array_angka) ) do
    begin
        i:=i+1;
        write('Input bilangan ke-',i,' : ');read(input_angka);
        array_angka[i-1]:=input_angka;
    end;
    writeln('=============================');
    
    x:=0;
    while(x < Length(array_angka) ) do
    begin
        nilai:=array_angka[x];
        x:=x+1;
        total_angka:=x*nilai;
        writeln(nilai,' x ', x,' = ',total_angka);
    end
end.